package pong.Game;
import javax.swing.JPanel;

import pong.Menu.Settings;


public class Player {

	Paddle paddle;
	int score;
	int number;
	
	int keyPress; //-1 == down 0 == no movement 1 == up
	
	public Player(int number){
		setScore(0);
		keyPress = 0;
		setNumber(number);
		
		paddle = new Paddle(number);
		setPaddleColor();
	}
	
	public void movePaddle(){
		switch (keyPress){
		case(1):
			paddle.movePaddleUp();
			System.out.println("P1 UP!!");
			break;
		case(-1):
			paddle.movePaddleDown();
		System.out.println("P1 DOWN!!");
			break;
		case(0):
			break;
		}
	}
	
	public void setPaddleColor(){
		switch(number){
			case(1):
				paddle.setColor(Settings.paddle1Color);
				break;
			case(2):
				paddle.setColor(Settings.paddle2Color);
				break;
		}
			
	}
	
	public Paddle getPaddle() {
		return paddle;
	}
	
	public int getNum() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getScore() {
		return score;
	}
	
	public void setKeyPress(int keyPress){
		this.keyPress = keyPress;
	}

	public void setScore(int score) {
		this.score = score;
	}
}