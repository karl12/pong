package pong.Game;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JComponent;

import pong.Menu.Settings;

public class Paddle extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int width = Settings.paddleWidth;
	private int height;
	private Color color;

	private Point center;

	public Paddle(int playerNum) {
		super();
		height = Settings.resolution.height / Settings.paddleHeightRatio;

		center = new Point();
		center.y = (Settings.resolution.height / 2) - (height / 2);
		center.x = 0;

		switch (playerNum) {
		case 1:
			// place paddle to the left
			center.x = width;
			break;
		case 2:
			// place paddle to the right
			center.x = Settings.resolution.width - (width * 2);
			break;
		}

		this.setBounds(center.x, center.y, width, height);
		resetPaddle();

		this.setVisible(true);
		repaint();
	}

	public void movePaddleUp() {

		if (getLocation().y > 0) {
			System.out.println(this.getLocation());
			setLocation(getLocation().x, getLocation().y - Settings.paddleSpeed);
		}
	}

	public void movePaddleDown() {
		if (getLocation().y < (Settings.resolution.height - height)) {
			System.out.println(getLocation().y);
			setLocation(getLocation().x, getLocation().y + Settings.paddleSpeed);
		}
	}

	protected void resetPaddle() {
		setLocation(center);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		this.setOpaque(true);

		Graphics g2d = (Graphics) g.create();
		g2d.setColor(color);
		g2d.fillRect(0, 0, width, height);
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(width, height);
	}
	
	public void setColor(Color color){
			this.color = color;
	}
}