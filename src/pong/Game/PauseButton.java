package pong.Game;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;


public class PauseButton extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7816677036016332002L;

	private boolean isPaused;
	
	PauseButton(){
		super();
		this.setText("Start");
		setPaused(true);
		this.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(isPaused()){
					setText("Pause");
					setPaused(false);
				}else{
					setText("Resume");
					setPaused(true);
				}
			}
		});
	}

	public boolean isPaused(){
		return isPaused;
	}

	public void setPaused(boolean isPaused) {
		setText("Resume");
		this.isPaused = isPaused;
	}
}