package pong.Game;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import pong.Menu.PongMenuBar;


public class TopBar extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7266850821263602574L;

	private JLabel score1;
	private JLabel score2;
	
	PauseButton pauseButton;
	
	TopBar(){
		super();
		this.setLayout(new FlowLayout());
		
		this.setFocusable(false);
		
		pauseButton = new PauseButton();
		
		score1 = new JLabel("Player 1: 0");
		score2 = new JLabel("Player 2: 0");
		
		this.add(score1, BorderLayout.WEST);
		this.add(pauseButton, BorderLayout.CENTER);
		this.add(score2, BorderLayout.EAST);	
	}
	
	public void setScores(int score1, int score2){
		this.score1.setText("Player 1: " + score1);
		this.score2.setText("Player 2: " + score2);
	}
}
