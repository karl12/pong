package pong.Game;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import pong.Menu.PongMenuBar;
import pong.Menu.Settings;
import pong.Menu.SettingsFrame;

public class Game extends JFrame{

	private static Board board;
	private TopBar topBar;
	private PongMenuBar menuBar;
	private JPanel boardBarContainer;
	private SettingsFrame settingsFrame;

	public Game(){
		super("Pong");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(Settings.resolution);
		this.setResizable(false);
		this.setBackground(Color.BLACK);
		
		this.setLayout(new BorderLayout());
		
		Container pane = this.getContentPane();
		
		settingsFrame = new SettingsFrame();
		
		board = new Board();
		topBar = new TopBar();
		boardBarContainer = new JPanel(new BorderLayout());
		menuBar = new PongMenuBar();
		
		boardBarContainer.add(topBar, BorderLayout.NORTH);
		boardBarContainer.add(board, BorderLayout.CENTER);
		
		this.add(menuBar, BorderLayout.NORTH);
		this.add(boardBarContainer, BorderLayout.CENTER);

		board.setVisible(true);
		topBar.setVisible(true);
		
		this.setVisible(true);
		
		menuBar.getNewGame().addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				board.startNewGame();
			}
			
		});
		
		menuBar.getSettings().addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				topBar.pauseButton.setPaused(true);
				settingsFrame.setVisible(true);
			}
			
		});
		
		this.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {
				System.out.println("Key Press: " + e.getKeyCode());
				switch (e.getKeyCode()) {
				case (Settings.player1Up):
					board.getPlayer1().keyPress = 1;
					break;
				case (Settings.player1Down):
					board.getPlayer1().keyPress = -1;
					break;
				case (Settings.player2Up):
					board.getPlayer2().keyPress = 1;
					break;
				case (Settings.player2Down):
					board.getPlayer2().keyPress = -1;
					break;
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				System.out.println("Key Released: " + e.getKeyCode());
				switch (e.getKeyCode()) {
				case (Settings.player1Up):
					board.getPlayer1().keyPress = 0;
					break;
				case (Settings.player1Down):
					board.getPlayer1().keyPress = 0;
					break;
				case (Settings.player2Up):
					board.getPlayer2().keyPress = 0;
					break;
				case (Settings.player2Down):
					board.getPlayer2().keyPress = 0;
					break;
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
				System.out.println("Key Typed: " + e.getKeyCode());
			}
		});
		
		new Runner().start();
		
	}
	
	public static void reDraw(){
		board.repaint();
	}
	
	public static void reColorPaddles(){
		board.player1.setPaddleColor();
		board.player2.setPaddleColor();
	}

	/** Thread running the animation. */
	private class Runner extends Thread
	{
	    /** Wether or not thread should stop. */
	    private boolean shouldStop = false;

	    /** Invoke to stop animation. */
	    public void shouldStop() {
	        this.shouldStop = true;
	    }//met

	    /** Invoke to pause. */
	    /** Main runner method : sleeps and invokes engine.play().
	     * @see Engine#play */
	    public void run()
	    {
	    	
	    	System.out.println("Runner running");
	        while( !shouldStop )
	        {
	            try {
	                Thread.sleep(18);
	            } catch (InterruptedException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }//catch
	            if( !topBar.pauseButton.isPaused() )
	            {
	            	board.player1.movePaddle();
	            	board.player2.movePaddle();
	            	board.ball.move(board.player1.getPaddle(), board.player2.getPaddle());
	            	board.ball.hasHitPaddle(board.player1);
	            	board.ball.hasHitPaddle(board.player2);
	            	board.offBoard();
	            	topBar.setScores(board.player1.getScore(), board.player2.getScore());
	            	board.repaint();
	            	requestFocus();
	            	
	            }//if
	        }//while
	    }//met

	    /** Wether or not we are paused. */
	    public boolean isPaused() {
	        return topBar.pauseButton.isPaused();
	    }//met
	}//class Runner (inner)
}


