package pong.Game;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JComponent;

import pong.Menu.Settings;

public class Ball extends JComponent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int speedX;
	private int speedY;
	
	private int radius;
	private Point center;
	private int maxX;
	private int maxY;
	
	public Ball() {
		super();
		
		radius = Settings.resolution.width/Settings.ballRatio;
		
		center = new Point();
		center.y = (Settings.resolution.height/2) - (radius/2);
		center.x = (Settings.resolution.width/2) - (radius/2);
		
		this.setBounds(center.x, center.y, radius, radius);
		
		maxX = Settings.resolution.width - (radius*2);
		maxY = Settings.resolution.height  - (radius*2);
		
		resetBall();
		this.setVisible(true);
	}
	
	protected void resetBall(){
		setLocation(center);
		
		double angle = 2 * Math.PI * Math.random();  // Random direction.

		if(angle > -0.25 && angle < 0.25) resetBall();
		if(angle > 0.75 && angle < 1.25) resetBall();
		
	    speedX = (int)Math.round(Math.cos(angle) * Settings.startSpeed);
	    speedY = (int)Math.round(Math.sin(angle) * Settings.startSpeed);
	}
	
	public void hasHitPaddle(Player player){
		Paddle p = player.getPaddle();
		int pPosX2 = p.getWidth() + p.getX();
		int pPosY2 = p.getHeight() + p.getY();
		
		Integer ballX = new Integer(getX());
		Integer ballY = new Integer(getY());
		
		ballY += (this.radius/2);
		
		if (player.getNum() == 2) ballX += radius;
		
		if (this.getY() >= p.getY() &&
				this.getY() <= pPosY2 &&
						ballX >= p.getX() &&
								ballX <= pPosX2){
			speedX *= -1;
		}
	}
	
	public void move(Paddle p1, Paddle p2){
		
		if (getY() <= 0 || 
				getY() >= (maxY)){
			speedY *= -1;
		}
		
		int newX = (int) Math.round(getLocation().x + speedX);
		int newY = (int) Math.round(getLocation().y + speedY);
		
		setLocation(newX, newY);
	}
	
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);

		this.setOpaque(true);
		Graphics g2d = (Graphics)g.create();
		g2d.setColor(Settings.ballColor);
		g2d.fillRect(0, 0, radius, radius);
	}
	
	@Override
    public Dimension getPreferredSize() {
        return new Dimension(radius,radius);
    }
}
