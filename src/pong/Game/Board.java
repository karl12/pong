package pong.Game;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JPanel;

import pong.Menu.Settings;

public class Board extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6609379587882582577L;
	Player player1;
	Player player2;

	Ball ball;

	Board() {
		super();
		this.setLayout(null);

		this.setBackground(Settings.boardColor);
		this.setPreferredSize(Settings.resolution);
		this.setBounds(0, 0, Settings.resolution.width,
				Settings.resolution.height);

		player1 = new Player(1);
		player2 = new Player(2);

		ball = new Ball();

		this.add(new JButton("test"));

		this.add(player1.getPaddle());
		this.add(player2.getPaddle());
		this.add(ball);

		System.out.println("Key Listener: " + this.getKeyListeners());

		startNewGame();

		this.setVisible(true);

		this.repaint();
		this.validate();

	}

	//0 = in bounds, 1 = player1 score, 2 = player2 score
	public void offBoard(){
		if (ball.getX() < 0){
			player2.setScore(player2.getScore() + 1);
			startNewRound();
		}else if (ball.getX() > this.getWidth() + ball.getWidth()){
			player1.setScore(player1.getScore() + 1);
			startNewRound();
		}
	}
	
	public void startNewGame() {
		ball.resetBall();
		player1.setScore(0);
		player2.setScore(0);
	}

	public void startNewRound() {
		ball.resetBall();
	}

	protected Player getPlayer1() {
		return player1;
	}

	protected Player getPlayer2() {
		return player2;
	}

	protected Ball getBall() {
		return ball;
	}

	@Override
	public void paintComponent(Graphics g){
		this.setBackground(Settings.boardColor);
		super.paintComponent(g);
	}
}
