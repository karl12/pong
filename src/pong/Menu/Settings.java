package pong.Menu;

import java.awt.Color;
import java.awt.Dimension;


public class Settings {
	public static int paddleHeightRatio = 5;
	public static Color paddle1Color = Color.GREEN;
	public static Color paddle2Color = Color.GREEN;
	public static int paddleWidth = 50;
	
	public static int ballRatio = 50;
	public static Color ballColor = Color.GREEN;
	public static double startSpeed = 10;
	
	public static Color boardColor = Color.RED;
	
	public static Dimension resolution = new Dimension(1900, 1000);
	
	public static final int player1Up = 81;
	public static final int player1Down = 65;
	
	public static final int player2Up = 79;
	public static final int player2Down = 76;
	
	public static final int paddleSpeed = 10;
}
