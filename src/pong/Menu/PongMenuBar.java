package pong.Menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class PongMenuBar extends JMenuBar {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1116354265681129539L;

	JMenu file;
	JMenuItem newGame;
	JMenuItem settings;
	
	public PongMenuBar(){
		super();
		
		file = new JMenu("File");
		newGame = new JMenuItem("New Game");
		settings = new JMenuItem("Settings");
		
		file.add(newGame);
		file.add(settings);
		this.add(file);
	}
	
	public JMenu getFile(){
		return file;
	}
	
	public JMenuItem getNewGame(){
		return newGame;
	}
	
	public JMenuItem getSettings(){
		return settings;
	}
}