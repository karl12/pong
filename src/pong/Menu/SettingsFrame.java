package pong.Menu;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import pong.Game.Game;

public class SettingsFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5400558384431644268L;

	final int height = 600;
	final int width = 800;
	
	JLabel p1ColorLabel;
	JLabel p2ColorLabel;
	JLabel ballColorLabel;
	JLabel backgroundColorLabel;
	
	JComboBox p1Color;
	JComboBox p2Color;
	JComboBox ballColor;
	JComboBox backgroundColor;
	
	JPanel p1Container;
	JPanel p2Container;
	JPanel ballContainer;
	JPanel backgroundContainer;
	
	public SettingsFrame(){
		super("Settings");
		this.setSize(width, height);
		this.setResizable(false);
		Container pane = getContentPane();
		pane.setLayout(new BoxLayout(pane, BoxLayout.PAGE_AXIS));
		
		p1ColorLabel = new JLabel("Player 1: ");
		p2ColorLabel = new JLabel("Player 2: ");
		ballColorLabel = new JLabel("Ball: ");
		backgroundColorLabel = new JLabel("Background: ");
		
		String[] colors = {"Red", "Blue", "Green", "Yellow"};
		
		p1Color = new JComboBox<String>(colors);
		p2Color = new JComboBox<String>(colors);
		ballColor = new JComboBox<String>(colors);
		backgroundColor = new JComboBox<String>(colors);
		
		p1Color.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox)e.getSource();
		        String color = (String)cb.getSelectedItem();
		        
		        if(color.equals("Red")){
		        	Settings.paddle1Color = Color.RED;
		        }else if(color.equals("Blue")){
		        	Settings.paddle1Color = Color.BLUE;
		        }else if(color.equals("Green")){
		        	Settings.paddle1Color = Color.GREEN;
		        }else if(color.equals("Yellow")){
		        	Settings.paddle1Color = Color.YELLOW;		        	
		        }
		        Game.reColorPaddles();
	        	Game.reDraw();
			}
			
		});
		
		p2Color.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox)e.getSource();
		        String color = (String)cb.getSelectedItem();
		        
		        if(color.equals("Red")){
		        	Settings.paddle2Color = Color.RED;
		        }else if(color.equals("Blue")){
		        	Settings.paddle2Color = Color.BLUE;
		        }else if(color.equals("Green")){
		        	Settings.paddle2Color = Color.GREEN;
		        }else if(color.equals("Yellow")){
		        	Settings.paddle2Color = Color.YELLOW;		        	
		        }
		        Game.reColorPaddles();
	        	Game.reDraw();
			}
		});
		
		ballColor.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox)e.getSource();
		        String color = (String)cb.getSelectedItem();
		        
		        if(color.equals("Red")){
		        	Settings.ballColor = Color.RED;
		        }else if(color.equals("Blue")){
		        	Settings.ballColor = Color.BLUE;
		        }else if(color.equals("Green")){
		        	Settings.ballColor = Color.GREEN;
		        }else if(color.equals("Yellow")){
		        	Settings.ballColor = Color.YELLOW;		        	
		        }
	        	Game.reDraw();
			}
		});
		
		backgroundColor.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox cb = (JComboBox)e.getSource();
		        String color = (String)cb.getSelectedItem();
		        
		        if(color.equals("Red")){
		        	Settings.boardColor = Color.RED;
		        }else if(color.equals("Blue")){
		        	Settings.boardColor = Color.BLUE;
		        }else if(color.equals("Green")){
		        	Settings.boardColor = Color.GREEN;
		        }else if(color.equals("Yellow")){
		        	Settings.boardColor = Color.YELLOW;		        	
		        }
	        	Game.reDraw();
			}
		});
		
		p1Container = new JPanel();
		p1Container.setLayout(new FlowLayout());
		p1Container.add(p1ColorLabel);
		p1Container.add(p1Color);

		p2Container = new JPanel();
		p2Container.setLayout(new FlowLayout());
		p2Container.add(p2ColorLabel);
		p2Container.add(p2Color);
		
		ballContainer = new JPanel();
		ballContainer.setLayout(new FlowLayout());
		ballContainer.add(ballColorLabel);
		ballContainer.add(ballColor);
		
		backgroundContainer = new JPanel();
		backgroundContainer.setLayout(new FlowLayout());
		backgroundContainer.add(backgroundColorLabel);
		backgroundContainer.add(backgroundColor);
		
		pane.add(p1Container);
		pane.add(p2Container);
		pane.add(ballContainer);
		pane.add(backgroundContainer);
	}
}
